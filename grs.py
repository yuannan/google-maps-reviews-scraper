#!/usr/bin/env python
# imports
import argparse
import yaml
import time
import os

# selenium
import selenium
import selenium.webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

# arg parser
parser = argparse.ArgumentParser()
parser.add_argument("-v", help="increase output [V]erbosity", action="store_true")
parser.add_argument("-u", help="single [U]RL to hit")
parser.add_argument("-f", help="read in from a file [F]ile the list of URLs to hit", action="store_true")
parser.add_argument("-t", help="[T]ime to sleep between between selenium scrolls, higher values are needed for slower machines/net speeds", type=int)
parser.add_argument("-d", help="run in [D]ebugging windowed mode", action="store_true")
parser.add_argument("-p", help="run in none [P]ersistant mode without cookies", action="store_true")
parser.add_argument("-l", help="run performance [L]ogger", action="store_true")
args = parser.parse_args()

# init Chromium selenium driver
options = selenium.webdriver.ChromeOptions()
options.add_argument("--lang=en")
# can also use "/usr/bin/google-chrome-stable" but I've used Ungoogled-Chromium
options.binary_location = "/usr/bin/chromium"
#options.add_argument("--remote-debugging-port=9515")
if not args.p: options.add_argument(r"user-data-dir=./cookies/") 
if not args.d: options.add_argument('headless')
driver = selenium.webdriver.Chrome(options=options)

# hit config
url = "https://www.google.com/maps/place/The+Golden+Chip/@51.0260399,-3.425173,10z/data=!4m11!1m2!2m1!1sthe+golden+chip!3m7!1s0x486df389964edd3d:0x61603b0d5e82edff!8m2!3d51.0260382!4d-3.1450484!9m1!1b1!15sCg90aGUgZ29sZGVuIGNoaXBaESIPdGhlIGdvbGRlbiBjaGlwkgEQY2hpbmVzZV90YWtlYXdheZoBI0NoWkRTVWhOTUc5blMwVkpRMEZuU1VOcGNYSkVibEpSRUFF"
sleep_time = 1
if args.u: url = args.u
if args.t: sleep_time = args.t
reviews_dir = "./reviews/"
URL_file = "hit_list.txt"
page_load_count = 10

if args.v:
    print("verbosity turned on")
    print("Default URL: " + url)
    print("Sleep Time: " + str(sleep_time))
    print("Windowed Mode: " + str(args.d))

def is_consent_URL():
    consent_url = driver.current_url
    consent_url_array = consent_url.split('/')
    return consent_url_array[2] == 'consent.google.com'

def hang_while_load(hang_url, timeout):
    if args.v: print("Hanging " + hang_url + " for " + str(timeout))
    for i in range(page_load_count):
        time.sleep(sleep_time)
        if hang_url == driver.current_url:
            if args.v: print("Arrived at URL!")
            break

def consent_form(hit_url):
    # this program will accept the Google terms and conditions if you do not wish for this to happen then edit this bit of code
    if is_consent_URL():
        if args.v: print("Google Consent Form detected!")
        agree_btn = driver.find_element_by_xpath("//body/c-wiz[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[4]/form[1]/div[1]/div[1]/button[1]")
        agree_btn.click()

def dump_db_to_yaml(hit_url, reviews_db):
    # make reviews_dir if not already made
    if not os.path.isdir(reviews_dir):
        os.mkdir(reviews_dir)
    # dumping review to a yaml file
    place_array = hit_url.split('/')
    place_name = place_array[5]
    place_cord = place_array[6]
    file_name = reviews_dir + place_name + place_cord + '.yaml'
    if args.v: print (file_name + ':\n' + str(reviews_db))
    with open(file_name, 'w') as file:
        yaml.dump(reviews_db, file)

def scrape_place(hit_url):
    # starting the hit
    driver.get(hit_url)
    time.sleep(sleep_time)

    if args.v: print(driver.current_url)
    if driver.current_url != hit_url:
        if args.v: print(f"This is not the desired URL, probably the Google consent form or slow page load. \n {hit_url=} \n {driver.current_url=}")
        consent_form(hit_url)

    # wait for page to load
    hang_while_load(hit_url, page_load_count)

    # init wait event with timeout of 10s
    menu_bt_wait = WebDriverWait(driver, 10)
    # wait until button can be found to be clicked
    menu_bt = menu_bt_wait.until(EC.element_to_be_clickable((By.XPATH, '//button[@data-value=\'Sort\']')))
    menu_bt.click()

    if args.v: print("Sorting menu deployed.")

    # click the recents button
    recent_wait = WebDriverWait(driver, 10)
    recent_rating_bt = menu_bt_wait.until(EC.element_to_be_clickable((By.XPATH,('/html[1]/body[1]/div[3]/div[3]/div[1]/ul[1]/li[2]'))))
    recent_rating_bt.click()

    if args.v: print("Sorting menu newest clicked.")

    time.sleep(sleep_time)

    if args.v: print("Wait for load over. attempting to scroll")
    # scroll to the bottom
    wait_rev_box = WebDriverWait(driver, 10)
    reviews_box = wait_rev_box.until(EC.element_to_be_clickable((By.XPATH, '/html[1]/body[1]/div[3]/div[9]/div[9]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]')))
    time.sleep(sleep_time)

    def scrollHeight():
        return driver.execute_script('return arguments[0].scrollHeight', reviews_box)

    def scrollTop():
        return driver.execute_script('return arguments[0].scrollTop', reviews_box)

    maxScrollHeight = -1
    currentScrollHeight = scrollTop()

    while currentScrollHeight != maxScrollHeight:
        if args.v: print(f'Current Height: {scrollTop()} Max Height: {scrollHeight()}')
        # set current height to the max height loaded
        driver.execute_script('arguments[0].scrollTop = arguments[0].scrollHeight', reviews_box)
        maxScrollHeight = currentScrollHeight
        currentScrollHeight = scrollTop()
        time.sleep(sleep_time)
    if args.v: print("Scrolled all the way!")

    # extraction of each review via relative xpath
    reviews_data = driver.find_elements(by=By.CLASS_NAME, value="jftiEf.L6Bbsd.fontBodyMedium")
    reviews_db = [dict() for i in range(len(reviews_data))]

    # loop through each review object and extract info
    for i in range(len(reviews_data)):
        review = reviews_data[i]
        # expand more if there exists
        more_button = review.find_elements(By.XPATH, './child::div[1]/div[3]/div[4]/jsl[1]/button[1]')
        for m in more_button:
            m.click()
        # star extraction
        stars_element = review.find_element(By.XPATH, './child::div[1]/div[3]/div[4]/div[1]/span[2]')
        stars_str = stars_element.get_attribute('aria-label')
        # make an array of the all the ints within the string
        star_int_array = [int(i) for i in stars_str.split(' ') if i.isdigit()]
        star_int = star_int_array[0]
        if args.v: print(star_int)
        # find and extract text within the span tag
        review_text = review.find_element(By.XPATH, './child::div[1]/div[3]/div[4]/div[2]/span[2]').text
        if args.v: print(review_text)
        review_dict = {
            "stars": star_int,
            "text": review_text
        }
        reviews_db[i] = review_dict

    dump_db_to_yaml(hit_url, reviews_db)

def start_scrape():
    if args.f:
        if args.v: print('[F]ile mode')
        with open(URL_file, 'r') as file:
            for l in file:
                raw_url = l.rstrip('\n')
                if args.v: print("Hitting URL: " + raw_url)
                scrape_place(raw_url)
    else:
        if args.v: print('[U]RL mode')
        scrape_place(url)

if __name__ == '__main__':
    if args.l:
        import cProfile
        import pstats

        with cProfile.Profile() as pr:
            start_scrape()
        stats = pstats.Stats(pr)
        stats.sort_stats(pstats.SortKey.TIME)
        stats.dump_stats(filename='perf.prof')
    else:
        start_scrape()
    
