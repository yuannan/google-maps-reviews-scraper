# Google Maps Reviews Scraper
This will use selenium to download all the reviews a place on Google Maps has

The URL provided **MUST** be the reviews page (click on the reviews link)

Cookies should be enabled to bypass the Google's Tracking Consent prompt. Not a big deal, just speeds it up a lil bit.

# Installation
- I recommend anaconda and creating a custom profile
## Anaconda
- conda create --name grs
- conda activate grs
- pip install -r requirements.txt

## On native runtime
- pip install -r requirements.txt

## Input
- Declare the URL for a single hit with -u
- Use file mode which will scrape all URLs in the file named "hit_list.txt" with -f

## Output
- Outputs will be in the directory called "reviews" by default
- Outputs are in YAML for easy human and machine readability

## Debugging
- pip install snakeviz to read "perf.prof"

## Notes
- A folder named 'reviews' will be created automatically if it's not already made. Reviews will be stored in the name of the shop + it's cordinates. This is extrapolated from the URL. This *should* be unique except in special cases where Google Maps may have a double entry with the same name and cords. In cases like this your reviews could be overwritten.
- Usernames and other personal information will **NOT** be collected due to privacy concerns. This can be easily extrapolated and added to the code base via few extra lines of code. This project will not and will never do this. I also politely ask you do not use my code for this. Please have the privacy of other users in mind when designing your own pipeline.
- Firefox is not used as Chromium simply provides better support for a lot of code. The ability to store cookies and extensions in an arbitrary directory are simply much better for data mining. I use Ungoogled-Chromium for this and the selenium driver works just fine.
